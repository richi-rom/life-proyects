import 'package:fireguardian/home.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return MultiBlocProvider(
      providers: [
        
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false, 
        initialRoute: "/",
        routes:{
          "/": (context) => const Home(),
        }, 
      ),
    );
  }
}

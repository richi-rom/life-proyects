


import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(size.longestSide*0.03),
                color: Colors.blue[300],
              ),
              height: size.height * 0.1,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Profile Page",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: size.longestSide*0.025,
                      fontWeight: FontWeight.bold
                    ),
                  )
                ],
              ),
            ),
            SizedBox(height:  size.height*0.01,),
            Container(
              height: size.height*0.78,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SingleChildScrollView(
                    child: Column(
                      children: [
                        Icon(Icons.person_pin, size: size.longestSide*0.2),
                        Text("Richi Romero", style: TextStyle(fontSize: size.longestSide*0.03, fontWeight: FontWeight.bold),),
                        SizedBox(
                          height: size.height * 0.03,
                        ),
                        Icon(Icons.location_on, size: size.longestSide*0.05, color: Colors.red),
                        Text("Xalapa, Ver. Mexico", style: TextStyle(fontSize: size.longestSide*0.02, fontWeight: FontWeight.bold),),
                        SizedBox(height:  size.height*0.05,),
                        Container(
                          width: size.width*1,
                          height: size.height*0.1,
                          decoration: BoxDecoration(
                            border: Border.all(color: Colors.black),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              SizedBox(width: size.width*0.02,),
                              Icon(Icons.person, size: size.longestSide*0.03,),
                              SizedBox(width: size.width*0.05,),
                              Text("Personal Information", style: TextStyle(fontSize: size.longestSide*0.02,),),
                              SizedBox(width:size.width*0.32,),
                              Icon(Icons.arrow_forward_ios, size: size.longestSide*0.03,),
                            ],
                          ),
                        ),
                       Container(
                          width: size.width*1,
                          height: size.height*0.1,
                          decoration: BoxDecoration(
                            border: Border.all(color: Colors.black),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              SizedBox(width: size.width*0.02,),
                              Icon(Icons.report, size: size.longestSide*0.03,),
                              SizedBox(width: size.width*0.05,),
                              Text("My Reports", style: TextStyle(fontSize: size.longestSide*0.02,),),
                              SizedBox(width:size.width*0.5,),
                              Icon(Icons.arrow_forward_ios, size: size.longestSide*0.03,),
                            ],
                          ),
                        ),
                        SizedBox(height:  size.height*0.05,),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      )
    );
  }
}
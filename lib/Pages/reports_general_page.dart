import 'package:fireguardian/widgets/item_report.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class ReportsGeneralPage extends StatefulWidget {
  const ReportsGeneralPage({Key? key}) : super(key: key);

  @override
  State<ReportsGeneralPage> createState() => _ReportsGeneralPageState();
}

class _ReportsGeneralPageState extends State<ReportsGeneralPage> {

  List list = [1,2,3,4,5,6,7,8,9,10];

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.white,
        child: Icon(Icons.report, color: Colors.red),
        onPressed: () {},
      ),
      body: Column(
        children: [
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(size.longestSide*0.03),
              color: Colors.blue[300],
            ),
            height: size.height * 0.1,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Reports Generals Page",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: size.longestSide*0.025,
                    fontWeight: FontWeight.bold
                  ),
                )
              ],
            ),
          ),
          SizedBox(height:  size.height*0.01,),
          Container(
            height: size.height*0.78,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ...list.map((e) => ItemReport()).toList()
                    ],
                  ),
                ),
              ],
            ),
          )
        ],
      )      
    );
  }
}
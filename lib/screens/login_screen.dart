import 'package:flutter/material.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
        body: Center(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.fireplace_outlined, 
                  size: 150.0,
                  color: Colors.red,
                ),
              SizedBox(height: 30.0),  
               Text(
                'SiGN IN',
                style: TextStyle(
                  fontSize: 24.0, 
                  fontWeight: FontWeight.bold, 
                  color: Colors.black, 
                  ),
                ), 
                 SizedBox(height: 20.0),  
                 Padding(
                  padding: EdgeInsets.all(16.0),
                  child: TextField(
                    decoration: InputDecoration(
                      labelText: 'Email',
                      border: OutlineInputBorder(),
                    ),
                  ),
                ),
                 Padding(
                  padding: EdgeInsets.all(16.0),
                  child: TextField(
                    decoration: InputDecoration(
                      labelText: 'Password',
                      border: OutlineInputBorder(),
                    ),
                    obscureText: true, 
                  ),
                ),
                 SizedBox(height: 30.0),  
                 Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10)
                  ),
                  width: size.width * 0.6,
                   child: MaterialButton(
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                    color: Colors.red,
                    height: size.height * 0.08,
                    onPressed: () {
                      print('Iniciar Sesión presionado');
                    },
                    child: Text('Login', style: TextStyle(
                fontSize: 24.0,
                color: const Color.fromARGB(255, 236, 232, 232), 
              ),), 
                               ),
                 ),
                SizedBox(height: 30.0), 
                Text(
                'Forgot Password?',
                style: TextStyle(
                  fontSize: 15.0, 
                  fontWeight: FontWeight.bold, 
                  color: Color.fromARGB(153, 0, 0, 0), 
                  ),        
            ),
            SizedBox(height: 20.0), 
            Text(
                'You do not have an account?',
                style: TextStyle(
                  fontSize: 18.0, 
                  fontWeight: FontWeight.bold, 
                  color: Color.fromARGB(183, 0, 0, 0), 
                  ),        
            )
            ],
            
                           
            ),
          ),
        ),
    
    );
  }
}
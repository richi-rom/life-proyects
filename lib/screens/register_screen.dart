import 'package:flutter/material.dart';

class Registerscreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
        body: Center(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
              SizedBox(height: 10.0),  
               Text(
                'Check In',
                style: TextStyle(
                  fontSize: 24.0, 
                  fontWeight: FontWeight.bold, 
                  color: Colors.black, 
                  ),
                ), 
                 SizedBox(height: 10.0),  
                 Padding(
                  padding: EdgeInsets.all(16.0),
                  child: TextField(
                    decoration: InputDecoration(
                      labelText: 'Name',
                      border: OutlineInputBorder(),
                    ),
                  ),
                ),
                 SizedBox(height: 10.0), 
                 Padding(
                  padding: EdgeInsets.all(16.0),
                  child: TextField(
                    decoration: InputDecoration(
                      labelText: 'Birthday',
                      border: OutlineInputBorder(),
                    ),
                    obscureText: true, 
                  ),
                ),
                 SizedBox(height: 10.0),  
                  Padding(
                  padding: EdgeInsets.all(16.0),
                  child: TextField(
                    decoration: InputDecoration(
                      labelText: 'Email',
                      border: OutlineInputBorder(),
                    ),
                    obscureText: true, 
                  ),
                ),
                 SizedBox(height: 10.0), 
                 Padding(
                  padding: EdgeInsets.all(16.0),
                  child: TextField(
                    decoration: InputDecoration(
                      labelText: 'Password',
                      border: OutlineInputBorder(),
                    ),
                    obscureText: true, 
                  ),
                ),
                 SizedBox(height: 10.0), 
                 Padding(
                  padding: EdgeInsets.all(16.0),
                  child: TextField(
                    decoration: InputDecoration(
                      labelText: 'Cell Phone Number',
                      border: OutlineInputBorder(),
                    ),
                    obscureText: true, 
                  ),
                ),
                 SizedBox(height: 10.0), 
                 
                 Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                   children: [
                    Checkbox(value: false, onChanged: (value){}),
                     Text(
                'Accept the terms and conditions',
                style: TextStyle(
                      fontSize: 15.0, 
                      fontWeight: FontWeight.bold, 
                      color: Color.fromARGB(171, 0, 0, 0),
                      decoration: TextDecoration.underline
                      ),        
            ),
                   ],
                 ),  
                 SizedBox(height: 40.0), 
                 Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10)
                  ),
                  width: size.width * 0.6,
                   child: MaterialButton(
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                    color: Colors.red,
                    height: size.height * 0.08,
                    onPressed: () {
                      print('Iniciar Sesión presionado');
                    },
                    child: Text('Sing In', style: TextStyle(
                fontSize: 24.0,
                color: const Color.fromARGB(255, 236, 232, 232), 
              ),), 
                    ),
                 ),
                 
              ],                
            ),
          ),
        ),
    
    );
  }
}
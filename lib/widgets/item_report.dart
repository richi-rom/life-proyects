



import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class ItemReport extends StatelessWidget {
  const ItemReport({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.only(bottom: size.longestSide*0.01),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(size.longestSide*0.02),
        color: Colors.blue[100]
      ),
      width: size.width * 0.8,
      padding: EdgeInsets.all(size.longestSide*0.01),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                child: Icon(Icons.image, size: size.longestSide*0.14),
              ),
              SizedBox(
                width: size.width*0.08,
              ),
              Container(
                child: Text(
                  "Report title",
                  style: TextStyle(
                    fontSize: size.longestSide*0.025,
                    fontWeight: FontWeight.bold
                  ),
                ),
              )
            ],
          ),
          Container(
            padding: EdgeInsets.all(size.longestSide*0.01),
            child: Text(
              """A fire is an uncontrolled occurrence of fire that can affect or burn something that is not intended to burn. It can affect structures and living beings."""
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Row(
                children: [
                  Icon(Icons.location_on, color:  Colors.red),
                  Text("Country Of Location")
                ],
              ),
              Row(
                children: [
                  Text("Classification"),
                  SizedBox(width: size.longestSide*0.01),
                  Text("D", style: TextStyle(color: Colors.red),)
                ],
              )
            ],
          )
        ],
        
      ),
    );
  }
}
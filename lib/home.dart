import 'package:fireguardian/Pages/profile_page.dart';
import 'package:fireguardian/Pages/reports_general_page.dart';
import 'package:fireguardian/Pages/reports_location_page.dart';
import 'package:flutter/material.dart';
class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {

  int selectPage = 1;
  PageController pageController = PageController(initialPage: 1);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      body: PageView(
        physics: NeverScrollableScrollPhysics(),
        controller: pageController,
        children: const [
          ReportsLocationsPage(),
          ReportsGeneralPage(),
          ProfilePage()
        ],
      ),
      bottomNavigationBar: Container(
        width: size.width * 1,
        height: size.height * 0.1,
        child: BottomAppBar(
          shape: const CircularNotchedRectangle(),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              GestureDetector(
                child: Icon(
                  Icons.newspaper, 
                  size: size.longestSide * 0.04,
                  color: selectPage == 0 ?Colors.blue : Colors.black,
                ),
                onTap: (){
                  setState(() {
                    pageController.jumpToPage(0);
                    selectPage = 0;
                  });
                },
              ),
              GestureDetector(
                child: Icon(
                  Icons.location_city, 
                  size: size.longestSide * 0.04,
                  color: selectPage == 1 ?Colors.blue : Colors.black,
                ),
                onTap: (){
                  setState(() {
                    pageController.jumpToPage(1);
                    selectPage = 1;
                  });
                },
              ),
              GestureDetector(
                child: Icon(
                  Icons.person, 
                  size: size.longestSide * 0.04,
                  color: selectPage == 2 ?Colors.blue : Colors.black,
                ),
                onTap: (){
                  setState(() {
                    pageController.jumpToPage(2);
                    selectPage = 2;
                  });
                },
              ),
            ],
          ),
        ),
      )
    );
  }
}